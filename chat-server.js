// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs"),
	user_info = [],
	rooms = [],
	owners = [],
	banned = [];
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.

	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.

		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket) {
	// This callback runs when a new Socket.IO connection is established.
	console.log("im here");

	//Listens for user-check from the client side
	socket.on('user-check', function(data) {
		var taken = false;
		for(var i in user_info){
			if(user_info[i][0] == data["username"]){
				taken = true;
			}
		}
		//If the username is already taken
		if(taken){
			io.to(socket.id).emit("name_taken");
		}
		//If the username is not taken, add it to array, add id to id array
		else{
			var userid = [data["username"], socket.id];
			user_info.push(userid);
			console.log(user_info);
			io.to(socket.id).emit("login_success");
		}
	});
	socket.on('logout', function(data){
		var index, inARoom = false, rName;
		//Find user info
		for(var i in user_info){
			if(user_info[i][1] = socket.id){
				index = i;
			}
		}
		//Check to see if client is in a room
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				inARoom = true;
				rName = rooms[j][0];
			}
		}
		if(inARoom){
			//Leave the room
			socket.leave(rName);
			io.to(rName).emit("someone-left", {user_left:user_info[index][0]});
		}
		//Delete user from users list
		user_info.splice(index, 1);
		console.log(user_info);
		io.to(socket.id).emit("logout_success");
	});
	socket.on('create-room', function(data){
		//Check to see if the room name is already being used
		var taken = false;
		for(var i = 0; i < rooms.length; i++){
			if(rooms[i][0] == data["room"]){
				taken = true;
			}
		}
		if(taken){
			io.to(socket.id).emit("room_name_taken");
		}
		else{
			//Find user name of client
			var name;
			for(var a in user_info){
				if(user_info[a][1] == socket.id){
					name = user_info[a][0];
				}
			}
			//Client joins room
			socket.join(data["room"]);
			//Make client the owner of the room
			var owner_info = [data["room"], socket.id];
			owners.push(owner_info);
			//Add room to rooms list
			var room_info = [data["room"], data["pass"]];
			rooms.push(room_info);
			//Send information back to client
			var creator = [];
			creator.push(name);
			io.sockets.emit("display-rooms", {rooms:rooms});
			io.to(data["room"]).emit("connect_room_success", {username:name, in_room:creator});
			io.to(socket.id).emit("send-message", {room:data["room"]});
		}
	});
	socket.on('join-room', function(data){
		//Check to see if the room exists
		var valid = false;
		var index;
		for(var i in rooms){
			if(rooms[i][0] == data["room"]){
				valid = true;
				index = i;
			}
		}
		if(valid){
			//Check to see if password entered is correct
			if(rooms[index][1] == data["pass"]){
				//Get username of client
				var name;
				for(var a in user_info){
					if(user_info[a][1] == socket.id){
						name = user_info[a][0];
					}
				}
				//Check to see if client has been banned from the room
				var bad = false;
				for(var b in banned){
					if(name == banned[b][0]){
						if(data["room"] == banned[b][1]){
							bad = true;
						}
					}
				}
				if(bad){
					io.to(socket.id).emit("banned-user");
				}
				else{
					//Join the room
					socket.join(data["room"]);
					//Find all users in the room
					var allUsersInRoom = [];
					for(var i in user_info){
						if(io.sockets.adapter.sids[user_info[i][1]][rooms[index][0]]){
							allUsersInRoom.push(user_info[i][0]);
						}
					}
					//Send information back to client
					io.to(data["room"]).emit("connect_room_success", {username:name, in_room:allUsersInRoom});
					io.to(socket.id).emit("send-message", {room:data["room"]});
				}
			}
			else{
				io.to(socket.id).emit("wrong_password");
			}
		}
		else{
			io.to(socket.id).emit("room_doesn't_exist");
		}
	});
	socket.on('leave-room', function(data){
		//Check to see if user is in a room
		var inRoom = false, index;
		for(var i in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[i][0]]){
				inRoom = true;
				index = i;
			}
		}
		//Get username from socket id
		for(var j in user_info){
			if(user_info[j][1] == socket.id){
				var userIndex = j;
			}
		}
		if(inRoom){
			//Leave the room
			socket.leave(rooms[index][0]);
			//Get all users in the room
			var allUsersInRoom = [];
			for(var i in user_info){
				if(io.sockets.adapter.sids[user_info[i][1]][rooms[index][0]]){
					allUsersInRoom.push(user_info[i][0]);
				}
			}
			//Send information back to clients
			io.to(rooms[index][0]).emit("someone-left", {user_left:user_info[userIndex][0], in_room:allUsersInRoom});
			io.to(socket.id).emit("left-room");
		}
		else{
			io.to(socket.id).emit("not-in-room");
		}
	});
	socket.on('send-message', function(data){
		var name, room;
		//Find username based off of socket id
		for(var i in user_info){
			if(user_info[i][1] == socket.id){
				name = i;
			}
		}
		//Find the room the user is in
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				room = j;
			}
		}
		//Send message to room
		io.to(rooms[room][0]).emit("room_message", {username:user_info[name][0], message:data["message"]});
	});
	socket.on('send-direct', function(data){
		//Find the room that the user is in
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				var roomIndex = j;
			}
		}
		//Get all users in the room
		var allUsersInRoom = [];
		var userInd;
		for(var i in user_info){
			if(io.sockets.adapter.sids[user_info[i][1]][rooms[roomIndex][0]]){
				var userAndId = [user_info[i][0], user_info[i][1]];
				allUsersInRoom.push(userAndId);
			}
			if(user_info[i][1] == socket.id){
				userInd = i;
			}
		}
		//Check to see if user submitted is in the room
		var receiver = false;
		for(var k in allUsersInRoom){
			if(allUsersInRoom[k][0] == data["user"]){
				receiver = true;
				var ind = k;
			}
		}
		//Send message to that user
		if(receiver){
			io.to(allUsersInRoom[ind][1]).emit("sent-direct", {receiver:data["user"], message:data["message"], sender:user_info[userInd][0]});
			io.to(allUsersInRoom[userInd][1]).emit("sent-direct", {receiver:data["user"], message:data["message"], sender:user_info[userInd][0]});
		}
		else{
			io.to(socket.id).emit("direct-failed");
		}
	});
	socket.on("kick-user", function(data){
		//Find what room the client is in
		var roomName;
		var roomIndex;
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				roomName = rooms[j][0];
				roomIndex = j;
			}
		}
		//Check that the person clicking the button is owner
		var owner = false;
		for(var i in owners){
			if(owners[i][0] == roomName){
				console.log(owners);
				if(socket.id == owners[i][1]){
					owner = true;
				}
			}
		}
		if(owner){
			//io.to(socket.id).emit("kick-success");
			//Get all users in the room
			var allUsersInRoom = [];
			for(var i in user_info){
				if(io.sockets.adapter.sids[user_info[i][1]][rooms[roomIndex][0]]){
					var userAndId = [user_info[i][0], user_info[i][1]];
					allUsersInRoom.push(userAndId);
				}
			}
			//Check to see if user submitted is in the room
			var badInRoom = false;
			var ind;
			for(var k in allUsersInRoom){
				if(allUsersInRoom[k][0] == data["username"]){
					badInRoom = true;
					ind = k;
				}
			}
			if(badInRoom){
				//io.to(socket.id).emit("bad-in-room");
				io.sockets.connected[user_info[ind][1]].leave(roomName);
				io.to(allUsersInRoom[ind][1]).emit("left-room");
				io.to(rooms[roomIndex][0]).emit("someone-left", {user_left:data["username"]});
			}
			else{
				io.to(socket.id).emit("bad-not-in-room");
			}
		}
		else{
			io.to(socket.id).emit("kick-fail");
		}
	});
	socket.on("ban-user", function(data){
		//Find what room the client is in
		var roomName;
		var roomIndex;
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				roomName = rooms[j][0];
				roomIndex = j;
			}
		}
		//Check that the person clicking the button is owner
		var owner = false;
		for(var i in owners){
			if(owners[i][0] == roomName){
				console.log(owners);
				if(socket.id == owners[i][1]){
					owner = true;
				}
			}
		}
		if(owner){
			//Check to see if the username submitted is real
			var real = false;
			var index;
			for(var i in user_info){
				if(user_info[i][0] == data["username"]){
					real = true;
					index = i;
				}
			}
			if(real){
				//Add user to banned users list
				var banned_user = [data["username"], roomName];
				banned.push(banned_user);
				//If that user is in the room, kick them out
				if(io.sockets.adapter.sids[user_info[index][1]][roomName]){
					io.sockets.connected[user_info[index][1]].leave(roomName);
					io.to(user_info[index][1]).emit("left-room");
					io.to(roomName).emit("someone-left", {user_left:data["username"]});
				}
				io.to(socket.id).emit("banned", {username:data["username"]});
			}
			else{
				io.to(socket.id).emit("not-real");
			}
		}
		else{
			io.to(socket.id).emit("kick-fail");
		}
	});
	socket.on("close-room", function(data){
		//Find name of the room
		var roomName;
		var roomIndex;
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				roomName = rooms[j][0];
				roomIndex = j;
			}
		}
		//Check that the person clicking the button is owner
		var owner = false;
		for(var i in owners){
			if(owners[i][0] == roomName){
				console.log(owners);
				if(socket.id == owners[i][1]){
					owner = true;
				}
			}
		}
		if(owner){
			//Find all users in the room
			var allUsersInRoom = [];
			for(var i in user_info){
				if(io.sockets.adapter.sids[user_info[i][1]][rooms[roomIndex][0]]){
					var userAndId = [user_info[i][0], user_info[i][1]];
					allUsersInRoom.push(userAndId);
				}
			}
			//Alert users in room that room is closing
			io.to(roomName).emit("room-closing");
			//Kick out all users in the room
			for(var j in allUsersInRoom){
				io.sockets.connected[allUsersInRoom[j][1]].leave(roomName);
				io.to(allUsersInRoom[j][1]).emit("left-room");
			}
			//Remove room from rooms list
			for(var k in rooms){
				if(roomName == rooms[k][0]){
					rooms.splice(k, 1);
				}
			}
		}
		else{
			io.to(socket.id).emit("kick-fail");
		}
	});
	socket.on("transfer", function(data){
		//Find name of the room
		var roomName;
		var roomIndex;
		for(var j in rooms){
			if(io.sockets.adapter.sids[socket.id][rooms[j][0]]){
				roomName = rooms[j][0];
				roomIndex = j;
			}
		}
		//Check that the person clicking the button is owner
		var owner = false;
		for(var i in owners){
			if(owners[i][0] == roomName){
				console.log(owners);
				if(socket.id == owners[i][1]){
					owner = true;
				}
			}
		}
		if(owner){
			//Check to see if the username submitted exists
			var real = false;
			var index;
			for(var i in user_info){
				if(user_info[i][0] == data["username"]){
					real = true;
					index = i;
				}
			}
			if(real){
				//Update owners list
				for(var a in owners){
					if(owners[a][0] == roomName){
						owners[a][1] = user_info[index][1];
					}
				}
			}
			else{
				io.to(socket.id).emit("not-real");
			}
		}
		else{
			io.to(socket.id).emit("kick-fail");
		}
	});
});