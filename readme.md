Two features for creative portion:  
1. Owner of chat room can close the room, which involves notifying everyone in the room that the room is closing, kicking the users out of the room, and then removing the room from the list of rooms  
2. Owner of chat room can transfer his ownership to another user, giving him full administrative control  
url: 52.89.36.182:3456

